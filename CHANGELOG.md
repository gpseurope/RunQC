# Update 06 December 2023 (by Juliette Legrand)
# README.md
update readme
# RunQC.pl
update RunQC.pl author and version

# Update 18 April 2023 (by Juliette Legrand)
## Util_Nav.pm
Cleaning of the different repository available to download the broadcast
"GOP_V3", "ROB_V3"  are the only repository that are still working. IGS and CDDIS are not available anymore in ftp. 
Allowing to download from there would necessitate new functions that are not compatible with what is available now.
In addition, with the experience we had of 1 year of monitoring at the DQMS, it showed that allowing only brdc from GOP is preferable and avoid to have QC that are not comparable from one node to the others.
In one year, BRDC orbits from GOPE have always been downloadable.
If necessaey it will be changed in the future, please contact epos@oma.be in case of trouble with the download of the brdc files. 
ROB_V3 has been added as backup of GOP_v3 (ROB_v3 can also be available in https)

## Util_Cfg.pm
change in xml config in anubis version 3.5, update of the function to create config file (not retro compatible)

## README.md
update information

