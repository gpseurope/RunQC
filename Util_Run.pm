# ----------------------------------------------------------------------------
# (c) 2018 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: run Anubis
#
# Created: 2018-08-09 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Run;

use strict;
use warnings;
use File::Basename;
use File::Path;
use Gps_Date;
use Util_Log;
use Util_Pro;

use Exporter;

@Util_Xqc::ISA    = qw(Exporter);
@Util_Xqc::EXPORT = qw(run_Anubis);

# Constructor
# ===========
sub new {
    my( $pkg  ) = shift;
    my( $log  ) = shift;
    my( $dbg  ) = shift;
    my( $db   ) = shift;
    my( $xqc  ) = shift || undef;
    my( $alr  ) = shift || undef;
    my( $api  ) = shift || undef;
 
    my $self =  {
        'log'   =>  $log,
        'dbg'   =>  $dbg,
        'xqc'   =>  $xqc,
        'alr'   =>  $alr,
        'api'   =>  $api,
        'db'    =>  $db
    };

    bless $self, $pkg;
}


# Function for running Anubis
# =========
sub run_Anubis{

  my $self     = shift;        my $log = $self->{'log'};
  my $time_lim = shift;        my $xqc = $self->{'xqc'};
  my $verb     = shift;        my $alr = $self->{'alr'};
  my $options  = shift;        my $api = $self->{'api'};
  my $command  = join ' ', @_;

  if( $command !~ /^s*$/ ){
    my $cfg_file = $options->{'conf_xml'} || "";

    $log->mesg("Util_Run",0,"G-Nut/Anubis start for $cfg_file [limit: ${time_lim}s]");
    $log->mesg("Util_Run",2,"$command");

    # always clean QC-XML before creating a new one!
    if( -s $options->{"file_xml"} ){ rmtree($options->{"file_xml"}) }
      
    # run Anubis to generate QC-XML
    my $irc = lim_exec($time_lim, $verb, "$command"); # irc is just text

    # if, from any reason, the QC-XML not created, generate a minimum QC-XML
    if( ! -s $options->{"file_xml"} ){
       $log->mesg("Util_Run",0,"GENERATING a minimum QC-XML: $options->{file_xml}");
      
       my $rec = $options->{'rec'};
       my $sit = uc($rec->{'station_name'});
       my $rxo = $rec->{"rxo_path"};                            # uncompressed file name
       my $crx = $rec->{"file_name"};                           #   compressed base name
       my $md5 = "";
       open( MD5, "md5sum $rxo |"); ( $md5 ) = <MD5> =~ /^(\S+)\s+$rxo/; close(MD5);
      
       if( $md5 eq "" ){
         $log->mesg("Util_Run",0,"Warning - cannot generate md5 for minimum QC-XML ($crx)");
       }else{
         open( QCXML, "> $options->{file_xml}" );
         my $qcxml;
            $qcxml .= "<QC_GNSS>\n";
            $qcxml .= " <meta>\n";
            $qcxml .= "  <created>".gps_date("-t -o %Y-%m-%dT%H:%M:%S")."</created>\n";
            $qcxml .= "  <program>".basename($0)."</program>\n";
            $qcxml .= "  <settings>\n";
            $qcxml .= "   <time_beg>".$rec->{'beg_time'}."</time_beg>\n";
            $qcxml .= "   <time_end>".$rec->{'end_time'}."</time_end>\n";
            $qcxml .= "   <sampling>".$rec->{'sampling'}."</sampling>\n";
            $qcxml .= "  </settings>\n";
            $qcxml .= " </meta>\n";
            $qcxml .= " <data></data>\n";    # EMPTY DATA FIELD (normally invalid)
            $qcxml .= " <head>\n";
            $qcxml .= "  <file_md5sum>".$md5."</file_md5sum>\n";
            $qcxml .= "  <file_name>".$crx."</file_name>\n";
            $qcxml .= "  <site_id>".$sit."</site_id>\n";
            $qcxml .= " </head>\n";
            $qcxml .= " </QC_GNSS>\n";
         printf QCXML "$qcxml";
         close( QCXML );
       }
    }

  }

  # Post QC_XML to DB-API Web Server
  if( defined $options && defined $api )
  {
    $api->post_QCXML($options->{'post_api'}, $options->{'file_xml'}, "file-mask");
  }
}


sub do_not_run_Anubis{

  my $self     = shift;        
  my $options  = shift;        
  my $log = $self->{'log'};  
  my $xqc = $self->{'xqc'};
  my $alr = $self->{'alr'};
  my $api = $self->{'api'};


       $log->mesg("Util_Run",0,"GENERATING a minimum QC-XML: $options->{file_xml}");
      
       my $rec = $options->{'rec'};
       my $sit = uc($rec->{'station_name'});
       my $rxo = $rec->{"rxo_path"};                            # uncompressed file name
       my $crx = $rec->{"file_name"};                           #   compressed base name
       my $md5 = "";
       open( MD5, "md5sum $rxo |"); ( $md5 ) = <MD5> =~ /^(\S+)\s+$rxo/; close(MD5);
      
       if( $md5 eq "" ){
         $log->mesg("Util_Run",0,"Warning - cannot generate md5 for minimum QC-XML ($crx) in case of no BRDC");
       }else{
         open( QCXML, "> $options->{file_xml}" );
         my $qcxml;
            $qcxml .= "<QC_GNSS>\n";
            $qcxml .= " <meta>\n";
            $qcxml .= "  <created>".gps_date("-t -o %Y-%m-%dT%H:%M:%S")."</created>\n";
            $qcxml .= "  <program>".basename($0)."</program>\n";
            $qcxml .= "  <settings>\n";
            $qcxml .= "   <time_beg>".$rec->{'beg_time'}."</time_beg>\n";
            $qcxml .= "   <time_end>".$rec->{'end_time'}."</time_end>\n";
            $qcxml .= "   <sampling>".$rec->{'sampling'}."</sampling>\n";
            $qcxml .= "  </settings>\n";
            $qcxml .= " </meta>\n";
            $qcxml .= " <data></data>\n";    # EMPTY DATA FIELD (normally invalid)
            $qcxml .= " <head>\n";
            $qcxml .= "  <file_md5sum>".$md5."</file_md5sum>\n";
            $qcxml .= "  <file_name>".$crx."</file_name>\n";
            $qcxml .= "  <site_id>".$sit."</site_id>\n";
            $qcxml .= " </head>\n";
            $qcxml .= " </QC_GNSS>\n";
         printf QCXML "$qcxml";
         close( QCXML );
       }
    

  

  # Post QC_XML to DB-API Web Server
  if( defined $options && defined $api )
  {
    $api->post_QCXML($options->{'post_api'}, $options->{'file_xml'}, "file-mask");
  }
}



1;
__END__
