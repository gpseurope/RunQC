# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: API handling
#
# Created: 2017-02-18 Petr Bezdeka
# Updated: 2017-10-17 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Api;

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

use strict;
use warnings;
use File::Path;
use File::Copy;
use File::Basename;
use LWP::UserAgent;
use IO::File;
# use Net::FTP;
# use Data::Dumper;

use lib dirname (__FILE__);
use Gps_Date;

use Exporter;

@Util_Api::ISA    = qw(Exporter);
@Util_Api::EXPORT = qw(post_QCXML);

# Constructor
# ===========
sub new {
  my( $pkg  ) = shift;
  my( $verb ) = shift;
  my( $log  ) = shift;
   
  my $self =  {
    'verb' =>  $verb,
    'log'  =>  $log,
    'pkg'  =>  $pkg
  };

  bless $self, $pkg;
}

# POST to DB API
# =============
sub post_QCXML()
{
  my $self     = shift;  my $log = $self->{'log'};
  my $urlapi   = shift;
  my $inp_file = shift;  # input files
  my $opt_meth = shift;  # file-list || file-mask

  my @files;             # temporary array of files

  # input method
  if(    lc($opt_meth) eq 'file-mask' ){ push @files, glob( "$inp_file" ); }
  elsif( lc($opt_meth) eq 'file-list' ){ open(FIL, "$inp_file");
                                         while(<FIL>){ chomp(); push( @files, $_ ) if -s "$_"; }
                                         close(FIL); 
                                       }
  else{ printf "post_QCXML: not proper method selected (file-list || file-mask)!\n"; }

  foreach my $qcxml ( @files )
  {
    my $strXML = "";

    if( defined $qcxml ){
        open(FILE, "$qcxml");
        while (my $line = <FILE> ){ $strXML .= $line; };
        close(FILE);
    }else{
      $log->warn($self->{'pkg'},2,"Error: QC-XML file not exist: $qcxml");  
    }

    my $url = "$urlapi"."/gps/data/qcfile";
    # my $json = '{"username": "foo", "password": "bar"}';

    my $ua = new LWP::UserAgent();
    my $response = $ua->post($url, Content => $strXML);
    
    if ( $response->is_success() ) {
        $log->mesg($self->{'pkg'},1,"Post QC-XML ($qcxml) to DB-API - SUCCESSFUL");
    }
    else {
        $log->warn($self->{'pkg'},0,"Post QC-XML ($qcxml) to DB-API - ERROR: " . $response->status_line());
        $log->warn($self->{'pkg'},0,"Response: " . $response->header("title")) if defined $response->header("title");
    }
  }
}

1;

__END__
