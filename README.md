# 1. Introduction #

The RunQC.pl script generates metadata on GNSS data quality in the QC_GNSS XML 
format (QC-XML). The G-Nut/Anubis software (https://software.pecny.cz/anubis)
is used for analyzing actual content of RINEX 2 and RINEX 3 files in 
term of quantitative, qualitative and complex data control. This version is compatible with Anubis version 3.9 (status 2024-01-07).

The script requires highly reliable and complete navigation messages.
These are automatically downloaded from the GOP historical archive (or
from the ROB backup server of GOP historical archive) providing globally merged,
checked and consolidated multi-GNSS navigation data. These files can be
locally stored (--brdc_dir) and not updated regularly (by default). The
update can be anytime triggered with a special option too (--brdc_upd). 
Alternatively, a local BRDC file can be directly used (--brdc_fil).

The script then uncompresses all input files (RINEXO+RINEXN),
and configures, starts and controls the Anubis software operation.
The resulted QC-XML file can be directly communicated to a Web
Service Server (fwss) for populating the EPOS DB developed in GNSS
Thematic Core Service (TCS). For the communication the user needs to 
define URL address of the service (--post_api).

The script can be started either in an interactive mode or non-interactively
from any other script. It can be run for three main purposes: 
1) generating QC-XML by checking the RINEX files, 
2) populating the EPOS DB with previuosly generated QC-XML files, 
or 3) providing both actions in a single sequence. Depending on the purpose, the input option requires a selection
of RINEX or QC-XML files and these cannot be combined in a single run
in any operation regime. In both cases, the input can be handled using a predefined
file list (--list_rxo vs. --list_xml) or a file mask (--mask_rxo vs. --mask_xml).
Alternatively, RINEX files could be processed when providing the JSON input 
(--json_inp) from the DB-API request (not implemented yet on DB-API).

The script is eventually designed to operate using a sequential (default) or
a parallel (--fork_run) mode. Any execution can be limited by a maximum time session
(e.g. --time_lim). In case of exceeding the limit, the script stops the Anubis and 
cleans temporary directories (--temp_dir).

The script supports a flexible command-line configuration of the Anubis (--conf_cmd),
however, this should be used with care as the DB-API expect a standard QC-XML file for
standard EPOS TCS data quality control. The output files such as QC-XML, QC-XTR, log and
configuration can be stored. Only the QC-XML output, used for the posting to the DB-API 
service, is saved by default, the others are saved only on demand (--save_xtr, --save_log,
--save_out). All output files are stored in defined output directory (--save_dir).

The script requires the enviroment variable PATH includes locations of the
Anubis and CRX2RNX software. The script also requires some specific perl libraries:

_JSON_ _(libxml-perl)_, _XML::LibXML_ _(libxml-libxml-perl)_,  _LWP::UserAgent_ _(libwww-perl)_.

* On systems using rpm/yum (RedHat, CentOS, Fedora, etc.): sudo yum install perl-JSON perl-libwww-perl "perl(XML::LibXML)" 
* and additionaly sudo yum install perl-JSON-XS"
* and additionaly sudo yum install perl-LWP-Protocol-https  openssl-devel  (due to unbudling of perl https..)
* On systems using dpkg/apt (Debian, Ubuntu, Mint, etc.):  sudo apt-get install sudo apt-get libjson-perl libwww-perl libxml-libxml-perl
*Or via cpan, etc.


**Initial Developers and Authors:** 
* Petr Bezdeka (petr.bezdeka@pecny.cz)
* Jan Dousa (jan.dousa@pecny.cz)

**Current Software maintainer**
* Juliette Legrand (juliette.legrand@oma.be)


**Contributors**

**Date created:** 18. January 2017

**Date last update:**
* 07 January 2024  (by Juliette Legrand) Better test the existence of the BRDC files and stop decimating RINEX at 30s and show the version 
* 06 December 2023 (by Juliette Legrand)
* 18 April 2023    (by Juliette Legrand)
# 2. Usage #

```bash
RunQC.pl

     --mask_rxo  string       .. mask for files in repository                     [MANDATORY]
     --list_rxo  string       .. list for files in repository                     [ALTERNATIVE]

     --date_ref  string       .. file reference time: "YYYY-MM-DD HH:MM:SS"       [OPTIONAL]

     --mask_xml  string       .. mask of existing QC-XML files                    [OPTIONAL] post_api is only called
     --list_xml  string       .. list of existing QC-XML files                    [OPTIONAL] post_api is only called

     --json_inp  string       .. input JSON file                                  [OPTIONAL]
     --json_out  string       .. store JSON file                                  [OPTIONAL]

     --post_api  string       .. if url, post QC_XML to DB-API Web server         [ACTIVATE the POST function, default:NONE]

     --brdc_dir  string       .. local path to brdc local archive                 [default:BRD/%Y]
     --brdc_fil  string       .. local path+name of brdc to be used prefferably   [default:""]
     --brdc_upd               .. update brdc files in local archive               [default:true]
     --pass_ftp               .. enforce passive ftp for brdc download            [default:false, i.e. implicite mode]

     --conf_cmd  string       .. configure Anubis from the command line string    [default:""]

     --time_lim  integer      .. time limit for single QC run [sec]               [default:99]
     --fork_lim  ingeger      .. run QC in parallel (int define max forks)        [default:0]
 
     --save_dir  string       .. output directory (may include gps_date format)   [default:LOG/%Y/%j]
     --save_log               .. save Anubis LOG file                             [default:false]
     --save_cfg               .. save Anubis CFG file                             [default:false]
     --save_xtr               .. save Anubis XTR file                             [default:false]
     --save_xml               .. save Anubis XML file                             [default:true]

     --temp_dir  string       .. temporary directory for uncompressed RINEX files [default:/tmp]
     
     --verb      integer      .. level of verbosity                               [default:1]
     --help                   .. help message                                     [default:false]
     --debug                  .. debug mode                                       [default:false]

```

### Mandatory parameters: ###

* --list_rxo: list of RINEX files to be checked (or to generate JSON input). Ex: `--list_rxo  my_file_list`

_OR_

* --mask_rxo: mask over RINEX files to be checked (or to generate JSON input). Ex: `--mask_rxo /data/center/2018/????/gope*.Z`

_OR_

* --list_xml: list of QC-XML files to be post to DB-API (can not be combined with QC run). Ex: `--list_xml /tmp/MY_LIST`

_OR_

* --mask_xml: mask over QC-XML files to be posted to DB-API (can not be combined with QC run). Ex: `--mask_xml /data/center/2018/???/*.xml`

_OR_

* --json_inp: input JSON input with request for RINEX files to be checked. Ex: `--json_inp testing.json`

```bash 
{"files":[{"file_name":"gope0040.97d.Z",
           "sampling":"30",
           "beg_time":"1997-01-04 00:00:00",
           "end_time":"1997-01-05 00:00:00",
           "local_path": "/kaca/ldc/CzechGeo/VESOG/1997/004",
           "md5_sum":"8ff8d53e4d881878d94e20488a05a3a7",
           "station_name":"gope"
         }
       ]}
```

_OR_

(not yet supported by Web Service Server)

* --json_api: url of Web Service Server to request JSON of unchecked files. Ex: `--json_api http://localhost:9001`
* --date_ref: request the start time of RINEX data to be asked by API. Ex: `--date_ref "YYYY-MM-DD HH:MM:SS"`


### Output and related settings: ###

* --post_api: url of Web Service Server to post QC_XML file to DB-API. Ex:` --post_api http://localhost:9001`

* --json_out: name of saved JSON (input) file which was generated from the file mask. Ex:` --json_out REQUEST_2.JS`

* --brdc_dir: change the root path of a local BRD archive maintained by the program. Ex: `--brdc_dir /data/center/BRDC/`
* --brdc_fil: prefferably use local BRDC file, if exists, don't download remote BRDC. Ex: `--brdc_file /local/brdc%j0.%yn.Z`
* --brdc_upd: requests to update broadcast data from the remote data centers. Ex: `--brdc_upd`

* --save_dir: change the root path of output directory for XML/XTR/LOG/CFG files. Ex: `--save_dir /data/center/BRDC/`
* --save_cfg: request to store Anubis' configuration file (CFG). Ex: `--save_cfg`
* --save_log: request to store Anubis' logging file (LOG). Ex: `--save_log`
* --save_xml: request to store Anubis' QC_XML file (XML). Ex: `--save_xml` (default!) or `--no-save_xml` (avoid saving!)
* --save_xtr: request to store Anubis' native QC  format (XTR). Ex: `--save_xtr`


##### Other optional settings #####

* --temp_dir: configure temporary directory for uncompressed RINEX files. Ex: `--temp_dir TMP'`

* --conf_cmd: configure G-Nut/Anubis from the command line for any specific user operation. Ex: `--conf_cmd ':gen:sys "GPS GLO" :outputs:verb=2'`

* --time_lim: configure maximum execution time[s] for QC run (the process is then killed). Ex: `--time_lim 99'`

* --fork_lim: configure maximum number of QC runs in parallel (0-1 is sequential mode). Ex: `--fork_lim 10'`


##### Other options #####
* --help: display help and exit.
* --verb: verbosity level for logging. Ex: `--verb 1`
* --debug: debug mode which mimics all operations and provide configurations only (no QC, no communication to Web Services Server or Database. Ex: `--debug`


# 3. Examples #

For the generating QC of requested files (without posting to DB-API) input JSON can be used as follows:

```bash
RunQC.pl  --json_inp  REQUEST.JS  --dir_brdc BRDC/%Y
```

For generating QC of RINEX data specified in file mask and posting to DB-API, the command line will be:

```bash
RunQC.pl  --mask_rxo "/data/center/network/2017/200/????????.??D.Z"  --brdc_dir BRDC/%Y --post_api http://drak.pecny.cz:9001
```

For generating QC of RINEX data specified in file list, no posting to DB-API, the command line will be:

```bash
RunQC.pl  --list_rxo "my_file_list" --brdc_dir BRDC/%Y --json_out REQUEST.JS
```

For generating QC of RINEX3 files specified by file mask, the command line will be:

```bash
RunQC.pl  --list_rxo "/data/center/network/2017/200/?????????_?_20172000000_01D_30S_MO.crx.gz"  --date_ref "2017-07-19 00:00:00" --brdc_dir BRDC/%Y --json_out REQUEST.JS
```

For generating QC of hourly RINEX2 data specified by file mask, the command line (hourly sequence) will be:

```bash
RunQC.pl  --mask_rxo "/data/center/network/2017/200/08/???????[Ii].??[Dd].Z"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/09/???????[Jj].??[Dd].Z"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/10/???????[Kk].??[Dd].Z"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/11/???????[Ll].??[Dd].Z"
```

For quality checking of hourly RINEX3 files specified by file mask, the command line (hourly sequence) will be:

```bash
RunQC.pl  --mask_rxo "/data/center/network/2017/200/08/?????????_?_20172000800_01D_30S_MO.crx.gz"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/09/?????????_?_20172000900_01D_30S_MO.crx.gz"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/10/?????????_?_20172001000_01D_30S_MO.crx.gz"
RunQC.pl  --mask_rxo "/data/center/network/2017/200/11/?????????_?_20172001100_01D_30S_MO.crx.gz"
```

For generating only JSON input request in a debug mode (e.g. for a later manual QC start), the command line will be:

```bash
RunQC.pl  --mask_rxo "/data/center/network/2017/200/????????.??D.Z" --json_out REQUEST.JS  --debug
```

For posting existing QC-XML to DB-API (without the QC run), the command line will be:

```bash
RunQC.pl  --mask_xml "/data/center/network/2017/200/*.xml" --post_api http://drak.pecny.cz:9001
```

For generating QC of RINEX files including an update of local broadcast data archive, the command line will be
(%Y, %j etc. are gps_date format time/date specification characters):

```bash
RunQC.pl  --json_inp REQUEST.JS  --brdc_dir BRDC/%Y  --brdc_upd
```

For generating QC of RINEX files with a special user settings for G-Nut/Anubis (e.g. removing augmentation systems such as SBAS, QZSS, IRNSS), the command line will be:

```bash
RunQC.pl  --json_inp REQUEST.JS  --conf_cmd ' :gen:sys "-SBS -QZS -IRN"'
```

For generating QC of RINEX files with storing Anubis' native XTR format including satellite-/time-specific data for skyplots and code
multipath, saving log file (verbosity level 2), the command line will be:

```bash
RunQC.pl  --json_inp REQUEST.JS  --conf_cmd ':qc:sec_ele=2 :qc:sec_mpt=2 :outputs:verb=2' --save_log --save_xtr
```

# 4. Limitations with current available version

* **IMPORTANT**: Handling a large data batch (e.g. starting single RunQC over many days or year)
  is strongly not recommended due to high demands on free space in TEMP directory at least
  (all files are downloaded and decompressed at the same time!). 
  This issue is specifically relevant when running RunQC in the Virtual Machine.
  
* **IMPORTANT**: Configuring RunQC for more parallel G-Nut/Anubis QC runs (--fork_lim != 1) is
  recommended only on machine where more processing cores are available. 
  The sequencial QC run (--fork_lim) is used by default.
  
* **IMPORTANT**: Configuring BRDC directory (--brdc_dir <PATH>) is suggested to enable about 5 GB of free space at least.
* **IMPORTANT**: Configuring SAVE directory (--save_dir <PATH>) is suggested to enable about 5 GB of free space at least.
  
* Currently, no (JSON) input from the Web Services Server is supported due to
  the lack of implementation of the corresponding DB-API service.

* It is recommended to operate RunQC better outside of the VM due to above reasons (CPUs, free space).

* Due to the need of navigation files, RINEX files should be processed
  based on a common date (e.g. for a common day of year).

* Support of daily 30s files (RINEX2 and RINEX3) when using long-term/short-term file/station names.

* High-rate data files are implicitly supported, but QC is performed like for 30s daily/hourly files.

* Handling hourly files (expected in near real-time) is performed
  without downloading navigation files, i.e. QC just use quantitative data checking.


# 5. Known Issues

* If there is a trouble with BRDC download, it is most likely a firewall issue
  when using active FTP, then try using passive FTP (--pass_ftp).

* TODO: handle other than FTP protocols.

#6. Synchronization Flags

File STATUS flag is defined by DB-API, but it has been described in the [QC funcionality
document](https://gitlab.com/gpseurope/RunQC/blob/master/doc/Specification-T3-functionality-v4.pdf)

|flag |action | description |
|-----|------ | ----------------------------- |
| -3  | none  | T3 vs T1 metadata incompliant |
| -2  | none  | QC not successful |
| -1  | none  | QC running |
|  0  | none  | File not checked |
|  1  | synchronize | QC completed – OK according to T1/T2/T3 metadata |
|  2  | synchronize | QC completed – minor inconsistency in RINEX header |
|  3  | synchronize | QC completed – warning flag based on hardwired QC metrics |
