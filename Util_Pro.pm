# ----------------------------------------------------------------------------
# (c) 2017 Geodetic Observatory Pecny, http://www.pecny.cz (gnss@pecny.cz)
#     Research Institute of Geodesy, Topography and Cartography
#     Ondrejov 244, 251 65, Czech Republic
#
# Purpose: fork and control program execution
#
# Created: 2014-10-01 Jan Dousa
# Updated: 2018-08-10 Jan Dousa
#
# ---------------------------------------------------------------------------
package Util_Pro;

require Exporter;

use File::Basename;
use POSIX  qw(:sys_wait_h);
use strict;

@Util_Pro::ISA    = qw( Exporter );
@Util_Pro::EXPORT = qw( lim_exec fork_func wait_subpids );


# -----------------
# >>> lim_exec <<<  (fork a process running exec for command)
# -----------------
sub lim_exec{
  my $time_lim = shift;
  my $verb     = shift; # verbosity
  my $command  = join ' ', @_;

  printf " command = $command  \n" if $verb > 4;

  my $pid = 0;
  my $sleep_count = 0;
  my $child_result = "";
    
  # fork process into Father & Child (until succeed)
  # ------------------------ -----------------------
  do {
    $pid = open(CHILD_OUT, "$command |");
    unless( defined $pid ){
      warn "cannot fork: $!"; die "bailing out" if $sleep_count++ > 6;  # max retries
      sleep 10; # sleep for a single retry
    }
  } until defined $pid;
    
  # distinct Farther & Child:
  # -------------------------
   
  # CHILD
  # -----
  if( $pid == 0 ){
     printf "$pid: FORK PARRENT successfull - child process started [%15.15s]\n", "$command" if $verb > 2;
     setpgrp(0,0);           # important for killing child processes  (at some systems?)!
     exit 0
  }
    
  # FARTHER
  # -------
  else{
    for( my $i=1 ; $i <= ${time_lim} ; $i++ ){
      if( -1 == waitpid( $pid, &WNOHANG ) ){ last } # child has finished
      sleep 1;
    }
    if( 0 == waitpid( $pid, &WNOHANG ) ){
      printf STDERR "$pid: CHILD killed due to time-limit [${time_lim}s] !\n" ;
      kill( 9, $pid );
    }
    while( <CHILD_OUT> ){ $child_result .= $_ }; close(CHILD_OUT);
    return $child_result;
  }    
}  


# --------------------
# >>> fork_func <<<   (fork an internal procedure running requested action)
# --------------------
sub fork_func {
  my $object   = shift;
  my $time_lim = shift;
  my $verb     = shift; # verbosity
  my $action   = shift;
  my( @input ) = @_;
  my $pid;

  FORK:{     
    if( $pid = fork ){}
    elsif( defined $pid )
    {
      # set alarm to kill this process if $time_lim > 0;
      if( defined $time_lim && $time_lim != 0 ){ alarm( ${time_lim} ) }
  	    
      # start action (routine)
#     { no strict 'refs'; &$action( @input );}
      { no strict 'refs'; eval $object->$action( @input ); }

      exit 0;	  
    }

    elsif( $! =~ /Resource temporarily unavailable/ )
    {
      printf STDERR " Couldn't create process, sleeping 5 \n";
      sleep 5;
      redo FORK;
    }

    else{ printf STDERR " Can't fork $! \n"; } # failure
  }
  return $pid;
}

# --------------------
# >>> wait_subpids <<<  (wait for subprocesses : getting, etc.)
# ====================
sub wait_subpids {

  my $time_lim = shift;
  my $verb     = shift; # verbosity
    
  # wait for all PIDs
  printf "\n Waiting for subprocesses:[dot=1s]\n |" if $verb > 1;
  for(my $i=1; $i <= 1 + $time_lim; $i++){

    # single-line output || 60ch-cut line output
    if(    $i%60 == 0 ){ printf "|" if $verb > 1; }
    elsif( $i%1  == 0 ){ printf "." if $verb > 1; }
          
    if( -1 == waitpid( -1, &WNOHANG ) ){
        printf " - stop waiting: %s [$$] !\n", basename($0) if $verb > 1;

      if( $i < $time_lim ){
        printf "\n Getting score [sec]: $i\n" if $verb > 1;
        return;

      }else{ last };
        
      sleep 1;
    }
      
    printf "=Max:${time_lim}sec]" if $verb > 1;
    printf "\n Getting score [sec]: $i [Max-time reached, ${time_lim} sec]\n" if $verb > 1;
  }
    
  printf "\n\n Waiting for subprocesses complated\n |" if $verb > 1;
}


1
__END__
